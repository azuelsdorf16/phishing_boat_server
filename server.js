//Copyright 2019 Andrew Zuelsdorf. All rights reserved.

//Libraries
const app = require("express")();
const http = require("http");
const fs = require("fs");
const cors = require("cors");
const bodyParser = require("body-parser");
const url = require('url');
const dns = require('dns');
const tldextract = require('tld-extract');

//Constants
const PORT = 8080;

app.use(bodyParser.urlencoded({"extended" : true}));
app.use(bodyParser.json());
app.use(cors());

//app.use(function (request, response) {
//    response.status(404).send({"error" : request.originalUrl + " not found."});
//});

app.get("/", function (request, response) {
    var domain = tldextract(request.query.url).domain;

    dns.lookup(domain, function (errors, results) {
	var responseData = {"domain" : domain};

        if (errors) {
            responseData.country = null;
            responseData.error = errors;
	}
	else {
            responseData.country = results;
            responseData.error = null;
	}

        response.send(responseData);
    });
});

//TODO: Implement https using Let's Encrypt.
//https.createServer(
//	{'key' : fs.readFileSync('key.pem'),
//	 'cert' : fs.readFileSync('cert.pem'),
//	 'passphrase' : "THINK OF A BETTER PASSPHRASE"
//	}, app).listen(PORT);

http.createServer(app).listen(PORT, 'localhost', function (err) {
    if (err) {
	console.error(err);
    }
    else {
        console.log("Started server on port ", PORT);
    }
});
